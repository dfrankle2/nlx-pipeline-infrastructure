output "subnets_created"{
    description = "Subnets created for nlx kubernetes"
    value = module.network_subnets.subnets
}