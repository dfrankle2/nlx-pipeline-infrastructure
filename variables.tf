variable "project_id" {
  description = "The project ID to host the cluster in"
  default = "sc-nlx"
}
variable "cluster_name" {
  description = "The name for the GKE cluster"
  default     = "nlx-cluster"
}
variable "env_name" {
  description = "The environment for the GKE cluster"
  default     = "dev"
}
variable "region" {
  description = "The region to host the cluster in"
  default     = "us-central1"
}
variable "network" {
  description = "The VPC network created to host the cluster in"
  default     = "default"
}
variable "subnetwork" {
  description = "The subnetwork created to host the cluster in"
  default     = "nlx-subnet"
}
variable "ip_range_pods_name" {
  description = "The secondary ip range to use for pods"
  default     = "nlx-pods-iprange"
}
variable "ip_range_services_name" {
  description = "The secondary ip range to use for services"
  default     = "nlx-services-iprange"
}