provider "google" {
  project = var.project_id
  region = var.region
}

# create subnets and secondary ip ranges in existing network
module "network_subnets" {
  source  = "terraform-google-modules/network/google//modules/subnets"
  version = "5.2.0"
  project_id  = var.project_id
  network_name = var.network

  subnets = [
      {
          subnet_name = "${var.subnetwork}-${var.env_name}"
          subnet_ip = "10.10.1.0/28"
          subnet_region = var.region
      },
  ]
  secondary_ranges = {
      "${var.subnetwork}-${var.env_name}" = [
          {
              range_name = var.ip_range_pods_name
              ip_cidr_range = "10.20.0.0/20"
          },
          {
              range_name = var.ip_range_services_name
              ip_cidr_range = "10.30.0.0/28"
          },
      ]
  }
}

#create kubernetes cluster
module "kubernetes_cluster"{
    source = "terraform-google-modules/kubernetes-engine/google"
    version = "22.1.0"

    #required inputs
    project_id = var.project_id
    network = var.network
    subnetwork = "${var.subnetwork}-${var.env_name}"
    name = var.cluster_name
    ip_range_pods = var.ip_range_pods_name
    ip_range_services = var.ip_range_services_name
    depends_on = [
      module.network_subnets
    ]

    #optional inputs
    remove_default_node_pool = true
    release_channel = "REGULAR"
    regional = false
    region = "us-central1"
    zones = ["us-central1-a", "us-central1-b", "us-central1-c"]
    create_service_account = false
    service_account = "651089442653-compute@developer.gserviceaccount.com"

    node_pools = [
        {
            name = "nlx-node-pool"
            image_type = "ubuntu_containerd"
            machine_type = "e2-standard-2"
            max_count = 3
            min_count = 1
            service_account = "651089442653-compute@developer.gserviceaccount.com"
            auto_upgrade = true
        }
    ]
}